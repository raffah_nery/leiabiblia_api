import express from 'express';
import bibles from './bibles';

const router = express.Router();

router.get('/', (req, res) => res.status(200).send({
    title: "Leia Bíblia - API Restful",
    version: "0.1.0.0"
}));

router.use('/bibles', bibles);

export default router;