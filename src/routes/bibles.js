import express from 'express';
import BiblesController from '../controllers/bibles';

const router = express.Router();
const controller = new BiblesController();

router.get('/', controller.getAll);
router.get('/:bible', controller.getBibleByShortName);
router.post('/', controller.post);
router.post('/:bible', controller.NewBible);

export  default router;