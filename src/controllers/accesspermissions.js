import UsersRepositories from '../repositories/users';
import Users from '../models/users';
import EmailService from '../services/email';
import AuthService from '../services/auth';

const repository = new UsersRepositories(Users);
const email = new EmailService();
const auth = new AuthService();