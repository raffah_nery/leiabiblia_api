import Bibles from '../models/bibles';
import BiblesRepositories from '../repositories/bibles';

const repBibles = new BiblesRepositories(Bibles);

class BiblesController {
    getAll = async(req, res) => {
        try {
            let data, response, bibles, books, chapters, verses, comments;

            data = await repBibles.all();
            bibles = [];

            for(let bible of data) {
                books = [];
                for(let book of bible.books) {
                    chapters = [];
                    for(let chapter of book.chapters) {
                        verses = [];
                        for(let verse of chapter.verses) {
                            comments = [];
                            for(let comment of verse.comments) {
                                comments.push({
                                    origin: comment.origin,
                                    author: comment.author,
                                    comment: comment.comment
                                })
                            }
                            verses.push({
                                number: verse.number,
                                verse: verse.verse,
                                comments: comments
                            })
                        }
                        chapters.push({
                            number: chapter.number,
                            description: chapter.description,
                            verses: verses
                        })
                    }
                    books.push({
                        name: book.name,
                        description: book.description,
                        chapters: chapters
                    })
                }
                bibles.push({
                    id: bible._id,
                    name: bible.name,
                    description: bible.description,
                    language: bible.language,
                    books: books
                });
            }

            response = {
                bibles: bibles
            };

            res.status(200).send(response);
        } catch(e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição'
            })
        }
    };
    getBibleByShortName = async(req, res) => {
        try {
            let bible_short_name, data;

            bible_short_name = req.params.bible;

            data = await repBibles.byName(bible_short_name);

            res.status(200).send(data)
        }
        catch (e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição'
            })
        }
    };
    /*createNewBible = async(req, res) => {
        let data;
        data = req.body;


    };*/
    post = async(req, res) => {
        try{
            let data;
            data = req.body;

            await repBibles.create(data);
            res.status(200).send({message: 'Cadastro realizado com sucesso!'})
        }
        catch (e) {
            res.status(500).send({
                e
            })
        }
    };
    NewBible = async(req, res) => {

    }
}

export default BiblesController;