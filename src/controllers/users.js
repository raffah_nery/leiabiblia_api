import UsersRepositories from '../repositories/users';
import Users from '../models/users';
import EmailService from '../services/email';
import AuthService from '../services/authenticate';

const repository = new UsersRepositories(Users);
const email = new EmailService();
const auth = new AuthService();

class UsersController {
    getAll = async(req, res) => {
        await repository.all()
            .then(response => {
                res.status(200).send(response.data);
            })
            .catch(error => {
                res.status(500).send({
                    message: "Houve um erro interno no servidor.",
                    data_error: error
                })
            });
    };
    getById = async(req, res) => {
        try {
            let user = await repository.single(req.params.id);
            if(user)
                throw Error;
            res.status(200).send(user);
        }catch (e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição'
            });
        }
    };
    /*getById = (req, res) => {
        return this.Product
            .findById(req.params.id)
            .then(data => {
                res.status(200).send(data);
            }).catch(error => {
            res.status(400).send(error);
        });
    };*/
    newUser = async(req, res) => {
        let user_email, data;

        user_email = req.body.email;

        data = await repository.create({
            users: [
                {
                    email: user_email
                }
            ]
        }).then(response => {
            return response
        }).catch(error => {
            return error
        });

        res.status(200).send({
            message: data
        })
    };
    post = async(req, res) => {
        try {
            let users = [];
            for(let user of req.body.users) {
                users.push({
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    password: md5(user.password + global.SALT_KEY)
                })
            }
            let data = await repository.create({
                name: req.body.name,
                business_name: req.body.business_name,
                type: req.body.type,
                cnpj: req.body.cnpj,
                cpf: req.body.cpf,
                status: req.body.status,
                inscricao_estadual: req.body.inscricao_estadual,
                ie_isento: req.body.ie_isento,
                inscricao_municipal: req.body.inscricao_municipal,
                cnae: req.body.cnae,
                regime: req.body.regime,
                address: req.body.address,
                phone: req.body.phone,
                email: req.body.email,
                website: req.body.website,
                platforms: req.body.platforms,
                users: users
            });
            //Envia e-mail de boas vindas
            email.send(data.email, 'Bem vindo ao HUBOH!', global.EMAIL_TMPL.replace('{0}', req.body.name));
            res.status(200).send({
                message: 'Cliente ' + data.name + ' foi cadastrado com sucesso!'
            });
        } catch(e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição',
                errors: e.errors
            });
        }
    };
    put = async(req, res) => {
        let sku = req.params.sku;
        this.Products
            .findOne({
                sku: sku
            })
            .then(data => {
                const id = data._id;
                return this.Products
                    .findByIdAndUpdate(id, {
                        $set: {
                            name: req.body.name
                            //Precisa terminar...
                        }
                    }).then(data => {
                        res.status(200).send({
                            message: 'sku: ' + data.sku + ' => alterado com sucesso!'
                        });
                    }).catch(error => {
                        res.status(400).send({
                            message: 'Falha ao atualizar produto => ' + error
                        });
                    })
            }).catch(error => {
            res.status(400).send({
                message: 'Sku não encontrado! => ' + error
            });
        });
    };
    authenticate = async(req, res) => {
        try {
            const user = await repository.authenticate({
                email: req.body.email,
                //password: md5(req.body.users.password + global.SALT_KEY)
            });
            //Verifica se encontrou registro.
            if(!user) {
                res.status(404).send({
                    message: 'Usuário não encontrado!'
                });
                return;
            }
            //Gera access token
            const token = await auth.genToken({
                id: user._id,
                email: user.email
            });
            //Resposta
            res.status(200).send({
                email: user.email,
                token: token
            });
        } catch(e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição'
            });
        }
    };
    delete = async(req, res) => {
        try {
            let data = await repository.remove(req.params.id);
            res.status(200).send({
                message: data.name + ' foi removido com sucesso!'
            });
        }catch (e) {
            res.status(500).send({
                message: 'Falha ao processar sua requisição'
            });
        }
    };
}

export default UsersController;