import mongoose from 'mongoose';

const schema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    email: String,
    address: {
        kind: String, // Descrição do endereço => Ex. Residencial. Comercial etc.
        street: String,
        number: String,
        complement: String,
        neighborhood: String,
        city: String,
        state: String,
        zip_code: String
    },
    contact: [
        {
            kind: String, // Fixo, Celular, Fax.
            number: String,
            obs: String // Observações.
        }
    ]
});

const Users = mongoose.model('Users', schema);

export default Users;