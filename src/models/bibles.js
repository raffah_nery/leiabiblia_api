import mongoose from 'mongoose';

const schema = new mongoose.Schema({
    name: String,
    short_name: String,
    description: String,
    language: String,
    books: [
        {
            name: String,
            description: String,
            abbreviation: String,
            testament: String,
            chapters: [
                {
                    number: Number,
                    subtitle: String,
                    description: String,
                    verses: [
                        {
                            number: Number,
                            text: String,
                            comments: [
                                {
                                    origin: String, // De onde vem o comentário (Livro).
                                    author: String,
                                    comment: String
                                }
                            ],
                            agreements: [
                                {
                                    book: String,
                                    chapters: [
                                        {
                                            number: Number,
                                            initial_verse: String,
                                            end_verse: String
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    subtitles: [
                        {
                            name: String,
                            agreements: [
                                {
                                    book: String,
                                    chapter: Number,
                                    initial_verse: String,
                                    end_verse: String
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});

const Bibles = mongoose.model('Bibles', schema);
export default Bibles;