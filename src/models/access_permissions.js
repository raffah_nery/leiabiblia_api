import mongoose from 'mongoose';

const schema = new mongoose.Schema({
    user_id: String,
    access_permissions: {
        bibles: {
            post: {
                create_new_bible: Boolean
            },
            get: {
                get_all: Boolean,
                get_bible_by_name: Boolean
            },
            put: {
                put_bible: Boolean
            },
            delete: {
                delete_bible: Boolean
            }
        }
    }
});

const AccessPermissions = mongoose.model('AccessPermissions', schema);

export default AccessPermissions;