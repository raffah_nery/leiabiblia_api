//Classe responsável pelos serviços de acesso a base de dados de forma assíncrona

class BiblesRepositories {
    constructor(Bibles) {
        this.Bibles = Bibles;
    };
    all = async() => {
        await this.Bibles
                .find().then(response => {
                return response
            }).catch(error => {
                throw new Error(error)
            });
    };
    byName = async(short_name)=> {
        let res = await this.Bibles
            .findOne({short_name: short_name})
            .then(response => ).catch();

        return res
    };
    create = async(data) => {
        let res = await this.Bibles(data)
            .save();

        return res;
    }
}

export default BiblesRepositories;