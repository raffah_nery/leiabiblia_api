//Classe responsável pelos serviços de acesso a base de dados de forma assíncrona

class UsersRepositories {
    constructor(Users) {
        this.Users = Users;
    };
    all = async() => {
        let res = await this.Users
            .find({}, {__v: 0});
        return res;
    };
    single = async(id) => {
        let res = await this.Users
            .findById(id, {__v: 0});
        return res;
    };
    create = async(data) => {
        let res = await this.Users(data)
            .save();
        return res;
    };
    remove = async(id) => {
        let res = await this.Users
            .findOneAndRemove({_id: id});
        return res;
    };
    authenticate = async(data) => {
        let res = await this.Users
            .findOne({
                email: data.email
            });
        return res;
    };
}

export default UsersRepositories;