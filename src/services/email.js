import config from '../../bin/config';
const sendgrid = require('sendgrid')(config.sendgridkey);

class EmailService {
    send = async (to, subject, body) => {
        sendgrid.send({
            to: to,
            from: 'hello@leiabibliafacil.com.br',
            subject: subject,
            html: body
        });
    };
}

export default EmailService;