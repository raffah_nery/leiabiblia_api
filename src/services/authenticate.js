import jwt from 'jsonwebtoken';

class AuthService {
    genToken = async (data) => {
        return jwt.sign(data, global.SALT_KEY);//{expiresIn: '1d'}
    };
    decodedToken = async (token) => {
        let decoded;
        try {
            decoded = await jwt.verify(token, global.SALT_KEY);
        } catch(e) {
            decoded = {error: e.message};
        }
        return decoded;
    };
    authorize = function (req, res, next) {
        let token = req.headers['x-access-token'];
        let email = req.headers['x-user-email'];

        if(!token || !email) {
            res.status(401).json({
                "error": "Para continuar, efetue login."
            });
        } else {
            jwt.verify(token, global.SALT_KEY, function(error, decoded) {
                if(error) {
                    res.status(401).json({
                        message: 'Token inválido.'
                    });
                } else if(decoded) {
                    if(decoded.email === email) {
                        next();
                    }else{
                        res.status(401).json({
                            message: 'email inválido.'
                        });
                    }
                }
            })
        }
    };
}

export default AuthService;