class Indexer {
    books = async(data) => {
        let bibles, books, chapters, verses, comments;
        for(let bible of data) {
            books = [];
            for(let book of bible.books) {
                chapters = [];
                for(let chapter of book.chapters) {
                    verses = [];
                    for(let verse of chapter.verses) {
                        comments = [];
                        for(let comment of verse.comments) {
                            comments.push({
                                origin: comment.origin,
                                author: comment.author,
                                comment: comment.comment
                            })
                        }
                        verses.push({
                            number: verse.number,
                            verse: verse.verse,
                            comments: comments
                        })
                    }
                    chapters.push({
                        number: chapter.number,
                        description: chapter.description,
                        verses: verses
                    })
                }
                books.push({
                    name: book.name,
                    description: book.description,
                    chapters: chapters
                })
            }
            bibles.push({
                id: bible._id,
                name: bible.name,
                description: bible.description,
                language: bible.language,
                books: books
            });
        }

        return bibles
    }
}
