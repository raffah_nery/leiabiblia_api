import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';
import database from '../bin/database';

const app = express();

const  configureExpress = () => {
    app.use(bodyParser.json());
    app.use('/', routes);

    return app;
};

// CORS.
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers','Origin, X-requested-With, Content-Type, Accept, x-access-token, x-user-email');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    next();
});

export default () => database.connect().then(configureExpress);