import mongoose from 'mongoose';

mongoose.Promise = Promise;

const mongoUrl = process.env.MONGO_URL || 'mongodb://root:vPBrt2df5mKJTNWu@ds151127.mlab.com:51127/leiabibliafacil_db';
const options = {socketTimeoutMS: 0, keepAlive: true, reconnectTries: 30};
const connect = () => mongoose.connect(mongoUrl, options);

export default {
    connect
}